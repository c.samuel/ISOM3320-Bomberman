import javafx.scene.image.ImageView;

public abstract class PowerUps {
	private int xcoord;
	private int ycoord;
	private int size; // number of pixels of each powerups on the screen
	private ImageView image;

	public PowerUps () {
		xcoord = ycoord = 0;
		size = 45;
	}

	public PowerUps (int xcoord, int ycoord, ImageView image) {
		this.xcoord = xcoord;
		this.ycoord = ycoord;
		size = 45;
		this.image = image;
	}

	public int getXcoord () {
		return xcoord;
	}

	public int getYcoord () {
		return ycoord;
	}

	public int getSize () {
		return size;
	}

	public ImageView getImage(){
		return image;
	}

	public void setXcoord (int xcoord) {
		this.xcoord = xcoord;
	}

	public void setYcoord (int ycoord) {
		this.ycoord = ycoord;
	}

	public void setSize (int size) {
		this.size = size;
	}

	public boolean isHit (Player p) {
            int x = p.getxpos();
            int y = p.getypos();
            int s = p.getsize();

        if (x==xcoord && y==ycoord && s==size) 	
        	return true;
        else
        	return false;
	}
}