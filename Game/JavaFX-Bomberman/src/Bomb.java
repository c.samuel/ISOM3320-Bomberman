import javafx.scene.image.ImageView;

public class Bomb{

    private int bombx;
    private int bomby;
    private int bombexplode;
    private String bombstatus;
    private ImageView image;

    Bomb(int bombx, int bomby, ImageView image){
            this.bombx = bombx;
            this.bomby = bomby;
            this.image = image;
    }

    Bomb(int bombx, int bomby){
            this.bombx = bombx;
            this.bomby = bomby;
    }

    public int getbombx(){
            return bombx;
    }

    public int getbomby(){
            return bomby;
    }

    public ImageView getImage(){
        return image;
    }
}