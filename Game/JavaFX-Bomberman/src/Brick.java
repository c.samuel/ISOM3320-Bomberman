import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Brick {
	private int xcoord;
	private int ycoord;
	private int size;
    private ImageView image;

	public Brick () {
            xcoord = ycoord = 0;
            size = 45;
	}

	public Brick (int xcoord, int ycoord, ImageView i) {
            this.xcoord = xcoord;
            this.ycoord = ycoord;
            this.size = 45;
            this.image = i;
	}

	public int getXcoord () {
		return xcoord;
	}

	public int getYcoord () {
		return ycoord;
	}

	public int getSize () {
		return size;
	}

	public void setXcoord (int xcoord) {
		this.xcoord = xcoord;
	}

	public void setYcoord (int ycoord) {
		this.ycoord = ycoord;
	}
	
	public void setSize (int size) {
		this.size = size;
	}
        
    public ImageView getImage(){
        return this.image;
    }
}
