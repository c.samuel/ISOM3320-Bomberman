import java.util.Scanner;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.io.BufferedReader;
import java.io.FileReader;
import javafx.scene.input.KeyCode;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import java.util.ArrayList;
import javafx.animation.Animation;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.util.Calendar;
import javafx.scene.image.ImageView;
import javafx.application.Platform;
import java.util.Timer;
import java.util.TimerTask;

class Player{

	Date today = new Date();
	Date today2;
	Date today3;
	Date today4;
	Date today5;

	
	private int xpos;
	private int ypos;
	private int speed;
	private int bombcount;
	private int score;
	private int bombsize;
	private int size;
	private int playerType;
	private ArrayList<Bomb> bombs;
	private boolean status = true;
	private ImageView image;
	private GameController gc;

	ArrayList<Integer> explodeYPos = new ArrayList<Integer>();
	ArrayList<Integer> explodeXPos = new ArrayList<Integer>();

	String left = "RIGHT";
	String bomb = "BOMB";


	Player() {
		bombs = new ArrayList<Bomb>();
	}

	Player(int xpos, int ypos, int playerType, int speed, int bombcount, int score, int size, ImageView img, GameController gc){
		this.xpos = xpos;
		this.ypos = ypos;
		this.playerType = playerType;
		this.speed = speed;
		this.bombcount = bombcount;
		this.score = score;
		this.size = size;
		this.bombs = new ArrayList<Bomb>();
		this.image = img;
		this.gc = gc;
	}

	public Bomb getBomb(int n){
		return bombs.get(n);
	}

	// if the action is walking, update the position if no brick exists
	// If the action is bombing, call placeBomb to handle the situation
	protected void walking(String Input){
		today2 = new Date();
		if (playerType == 1) {

			if (Input.equals("UP") && ypos - 45 >= 0 && !gc.isBrickExist(xpos, ypos-45)) {

				this.updateypos(-45);

				System.out.println ("up");
			}
			else if (Input.equals("DOWN") && ypos + 45 <= 630 && !gc.isBrickExist(xpos, ypos+45)) {
				this.updateypos(45);

				System.out.println ("down");
			}
			else if (Input.equals("LEFT") && xpos - 45 >= 0 && !gc.isBrickExist(xpos-45, ypos)) {
				this.updatexpos(-45);

				System.out.println ("left");
			}
			else if (Input.equals("RIGHT") && xpos + 45 <= 630 && !gc.isBrickExist(xpos+45, ypos)) {
				this.updatexpos(45);

				System.out.println ("right");
			}
			if (Input.equals("ENTER")){

				System.out.println("Pressed enter");
				
				placeBomb(xpos, ypos);
			}

		}
		else if (playerType == 2 ){
			System.out.println("player type = 2" );

			if (Input.equals("W") && ypos - 45 >= 0 && !gc.isBrickExist(xpos, ypos-45)) {
				this.updateypos(-45);
				// System.out.println ("up");
			}
			else if (Input.equals("S") && ypos + 45 <= 630 && !gc.isBrickExist(xpos, ypos+45)) {
				this.updateypos(45);
				// System.out.println ("down");
			}
			else if (Input.equals("A") && xpos - 45 >= 0 && !gc.isBrickExist(xpos-45, ypos)) {
				this.updatexpos(-45);
				// System.out.println ("left");
			}
			else if (Input.equals("D") && xpos + 45 <= 630 && !gc.isBrickExist(xpos+45, ypos)) {
				this.updatexpos(45);
				// System.out.println ("right");
			}
			else if (Input.equals("C")){

				placeBomb(xpos, ypos);
			}

		}
	}

	// place the bomb into the screen, add the explosion range of the bomb into add explodePos
	// and count 3 seconds before using the bomb
	public void placeBomb(int x, int y){
		System.out.println("bomb.size() = "+bombs.size());
		System.out.println("getbombcount = "+bombcount);
		if(bombs.size() < (this.getbombcount())){
			bombs.add(new Bomb(x, y));
			System.out.println("bomb.size() after place bomb = "+bombs.size());
			gc.addBomb(x, y);
			addExplosionArea(xpos, ypos);
			new Timer().schedule( 
				new TimerTask() {
					public void run() {
						Platform.runLater(new Runnable() { // need to use platform.runlater to update the UI in separate thread
							public void run() {
								useBomb();
							}
						});
					}
				}, 
				3000 
			);
		}
	}

	// remove bomb from the player.bombs, call screen to handle the explosion case
	public void useBomb(){
		System.out.println("use bomb");
		System.out.println("removing bomb x: "+ bombs.get(0).getbombx() + " ; y: "+ bombs.get(0).getbomby());

		gc.handleExplode(explodeXPos, explodeYPos, bombs.get(0).getbombx(), bombs.get(0).getbomby());
		// gc.checkPlayerDie(xpos, ypos);
		bombs.remove(0);
	}

	// add explosion area to the ArrayList explodeX and explodeY (for the use of GameController)
	protected void addExplosionArea (int xpos, int ypos){
		int a = ypos;
		int b = xpos;
		int[] position = {a, b};

		if (position[0]+45<586) {
			explodeYPos.add(position[0]+45);
			explodeXPos.add(position[1]);
		}

		if (position[0]-45>-1){
			explodeYPos.add(position[0]-45);
			explodeXPos.add(position[1]);
		}

		if (position[1]+45<586){
			explodeYPos.add(position[0]);
			explodeXPos.add(position[1]+45);
		}

		if (position[1]-45>-1){
			explodeYPos.add(position[0]);
			explodeXPos.add(position[1]-45);
		}
		explodeYPos.add(position[0]);
		explodeXPos.add(position[1]);
	}

	protected void setxpos(int xpos){
		this.xpos = xpos;
	}

	protected void setplayertype (int playerType){
		this.playerType = playerType;
	}

	protected void setypos(int ypos){
		this.ypos = ypos;
	}

	protected void setsize(int size){
		this.size = size;
	}

	protected void setspeed(int speed){
		this.speed = speed;
	}

	protected void setbombcount(int bombcount){
		this.bombcount = bombcount;
	}

	protected void setscore(int score){
		this.score = score;
	}

	protected void setbombsize(int bombsize){
		this.bombsize = bombsize;
	}

	protected void setImage(ImageView image){
		this.image = image;
	}

	public int getxpos(){
		return xpos;
	}

	public int getypos(){
		return ypos;
	}

	public int getspeed(){
		return speed;
	}

	public int getbombcount(){
		return bombcount;
	}

	public int getscore(){
		return score;
	}

	public int getsize(){
		return size;
	}

	protected void updatespeed(){
		speed++;
	}

	protected void updatebombcount(){
		bombcount++;
		System.out.println("bombcount == "+bombcount+" now");
	}

	protected void updatescore(){
		score++;
	}

	protected void updatexpos(int updatexpos){
		this.xpos = this.xpos + updatexpos;
		gc.updatePlayerLocation(this);
	}

	protected void updateypos(int updateypos){
		this.ypos = this.ypos + updateypos;
		gc.updatePlayerLocation(this);
	}

	protected void updatesize(){
		size++;
	}

	public ImageView getImage(){
		return this.image;
	}

}