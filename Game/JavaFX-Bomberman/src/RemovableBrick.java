import javafx.scene.image.ImageView;

public class RemovableBrick extends Brick {
	
    public RemovableBrick () {
		super();
	}

    public RemovableBrick (int xcoord, int ycoord, ImageView i) {
        super(xcoord, ycoord, i);
    }   

}