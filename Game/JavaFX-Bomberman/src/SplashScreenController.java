import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class SplashScreenController implements Initializable {

    @FXML
    private Button playButton;

    // Handle the action when user clicks button on SplashScreen
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        Stage stage; 
        Parent root;
        
        Button btn = (Button) event.getSource();
        String id = btn.getId();

        switch(id){
            case "playButton": // when the user clicks the playButton, start the game interface
                //get reference to the button's stage         
                stage = (Stage) btn.getScene().getWindow();
                //load up game FXML document
                root = FXMLLoader.load(getClass().getResource("GameScreen.fxml"));
                
                //create a new scene with root and set the stage
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                break;
            default:
                break;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("initialize splash screen");
    }
    
}
