import javafx.animation.AnimationTimer;

public class GamePlayLoop extends AnimationTimer {
    
    protected GameController gameController;
    private boolean playStatus = true;

    public GamePlayLoop(){}

    public GamePlayLoop(GameController gc){
        this.gameController = gc;
    }
    
    // Call sceneEventHandling in gamecontroller continuously
    @Override
    public void handle(long now) {
        if (playStatus == true)
            gameController.sceneEventHandling();
    }
    
    @Override
    public void start(){
        super.start();
    }
    
    @Override
    public void stop(){
        System.out.println("Stopped");
        playStatus = false;
        super.stop();
    }
}